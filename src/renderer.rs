use core::mem;
use std::borrow::Cow;
use std::sync::Arc;
use vulkano::buffer::{BufferUsage, CpuAccessibleBuffer, CpuBufferPool};
use vulkano::buffer::immutable::ImmutableBuffer;
use vulkano::command_buffer::{
    AutoCommandBufferBuilder, CommandBufferUsage, DynamicState, SubpassContents,
};
use vulkano::descriptor_set::{FixedSizeDescriptorSetsPool, PersistentDescriptorSet};
use vulkano::descriptor_set::layout::{DescriptorBufferDesc, DescriptorDesc, DescriptorDescTy, DescriptorSetLayout};
use vulkano::descriptor_set::pool::{StdDescriptorPool, DescriptorPool};
use vulkano::device::physical::{PhysicalDevice, PhysicalDeviceType};
use vulkano::device::{Device, DeviceExtensions, Features, Queue};
use vulkano::image::swapchain::SwapchainImage;
use vulkano::image::view::ImageView;
use vulkano::image::{AttachmentImage, ImageUsage};
pub use vulkano::instance::Version;
use vulkano::instance::{ApplicationInfo, Instance};
use vulkano::instance::debug::DebugCallback;
use vulkano::pipeline::vertex::BuffersDefinition;
use vulkano::pipeline::viewport::Viewport;
use vulkano::pipeline::{GraphicsPipeline, GraphicsPipelineAbstract};
use vulkano::pipeline::shader::ShaderStages;
use vulkano::render_pass::{Framebuffer, FramebufferAbstract, RenderPass, Subpass};
use vulkano::swapchain;
use vulkano::swapchain::{AcquireError, Surface, Swapchain, SwapchainCreationError};
use vulkano::sync::{FlushError, GpuFuture};
use vulkano::format::Format;
use winit::event::VirtualKeyCode::P;
use winit::window::Window;
use crate::camera::{Camera, ModelViewProjection};
use crate::mesh;
use crate::mesh::MeshInstance;

mod vs {
    vulkano_shaders::shader! {
                    ty: "vertex",
                    path: "src/shaders/tri.vert"
                }
}

mod fs {
    vulkano_shaders::shader! {
                    ty: "fragment",
                    path: "src/shaders/tri.frag"
                }
}

#[allow(dead_code)]
pub struct Renderer {
    instance: Arc<Instance>,
    surface: Arc<Surface<Arc<Window>>>,
    device: Arc<Device>,
    queue: Arc<Queue>,
    swapchain: Arc<Swapchain<Arc<Window>>>,
    images: Vec<Arc<SwapchainImage<Arc<Window>>>>,
    render_pass: Arc<RenderPass>,
    pipeline: Arc<GraphicsPipeline<BuffersDefinition>>,
    dynamic_state: DynamicState,
    framebuffers: Option<Vec<Arc<dyn FramebufferAbstract + Send + Sync>>>,
    should_recreate_swapchain: bool,
    previous_frame_end: Option<Box<dyn GpuFuture + 'static>>,
    descriptor_pool: FixedSizeDescriptorSetsPool,
    mvp_buffer_pool: CpuBufferPool<vs::ty::MVPMatrices>,
}

vulkano::impl_vertex!(mesh::Vertex, position);

impl Renderer {
    pub fn init(
        app_name: Option<Cow<'_, str>>,
        app_version: Option<Version>,
        window: Arc<Window>,
    ) -> Self {
        /*
        Initializes the renderer.
        */

        // TODO(REF): Split this function
        let engine_name = Some(Cow::Borrowed("Frobenius"));
        let engine_version = Version {
            major: 0,
            minor: 0,
            patch: 1,
        };


        let mut layers: Option<&str> = None;
        if cfg!(debug_assertions) && vulkano::instance::layers_list().unwrap().find(|l| l.name() == "VK_LAYER_KHRONOS_validation").is_some() {
            println!("Enabling validation layer.");
            layers = Some("VK_LAYER_KHRONOS_validation");
        }

        // Create Vulkan instance
        let instance = {
            let app_info = ApplicationInfo {
                application_name: app_name,
                application_version: app_version,
                engine_name: engine_name,
                engine_version: Some(engine_version),
            };

            let extensions = vulkano_win::required_extensions();

            Instance::new(Some(&app_info), Version::V1_2, &extensions, layers)
                .expect("Failed to create instance.")
        };

        if cfg!(debug_assertions) {
            let _callback = DebugCallback::errors_and_warnings(&instance, |msg| {
                println!("Debug callback: {:?}", msg.description);
            });
        }

        // Create draw surface
        let surface = vulkano_win::create_vk_surface(window, instance.clone())
            .expect("Could not create draw surface");

        // We need the swapchain extension and khr_portability_subset
        // when running using MoltenVK on macOS
        let using_moltenvk = cfg!(target_os = "macos");
        let device_extensions = DeviceExtensions {
            khr_swapchain: true,
            khr_portability_subset: using_moltenvk,
            ..DeviceExtensions::none()
        };

        // Print available physical devices in debug builds
        if cfg!(debug_assertions) {
            for physical_device in PhysicalDevice::enumerate(&instance) {
                println!("{}", physical_device.properties().device_name);
            }
        }

        // Select physical device and queue family
        let (physical_device, queue_family) = PhysicalDevice::enumerate(&instance)
            .filter(|&p| p.supported_extensions().is_superset_of(&device_extensions))
            .filter_map(|p| {
                // Select queue family for device p
                p.queue_families()
                    .filter(|&f| f.supports_graphics() && surface.is_supported(f).unwrap_or(false))
                    .max_by_key(|&f| f.queues_count())
                    .map(|q| (p, q))
            })
            .min_by_key(|(p, _)| match p.properties().device_type {
                PhysicalDeviceType::DiscreteGpu => 0,
                PhysicalDeviceType::IntegratedGpu => 1,
                PhysicalDeviceType::VirtualGpu => 2,
                PhysicalDeviceType::Cpu => 3,
                PhysicalDeviceType::Other => 4,
            })
            .expect("Could not find a combination of physical device and queue family that supports the requirements.");

        println!("Using device: {}", physical_device.properties().device_name);
        // Create Vulkan device and get queue
        let (device, mut queues) = {
            Device::new(
                physical_device,
                &Features::none(),
                &device_extensions,
                [(queue_family, 0.5)].iter().cloned(),
            )
            .expect("Failed to create Vulkan device.")
        };

        let queue = queues.next().unwrap();

        // Create swapchain
        let (swapchain, images) = {
            let caps = surface.capabilities(physical_device).unwrap();
            let dimensions = caps.current_extent.unwrap_or([1280, 1024]);
            let alpha = caps.supported_composite_alpha.iter().next().unwrap(); // TODO: Check this
            let format = caps.supported_formats[0].0; // TODO: Check this

            Swapchain::start(device.clone(), surface.clone())
                .num_images(caps.min_image_count) // TODO: Check this. Double bufffer? Triple buffer?
                .format(format)
                .dimensions(dimensions)
                .usage(ImageUsage::color_attachment())
                .composite_alpha(alpha)
                .sharing_mode(&queue)
                .build()
                .expect("Failed to create swapchain.")
        };

        // Define render pass
        let render_pass = Arc::new(
            vulkano::single_pass_renderpass!(device.clone(),
            attachments: {
                color: {
                    load: Clear,
                    store: Store,
                    format: swapchain.format(),
                    samples: 1,
                },
                depth: {
                    load: Clear,
                    store: DontCare,
                    format: Format::D16Unorm,
                    samples: 1,
                }
            },
            pass: {
                color: [color],
                depth_stencil: {depth}
            })
            .unwrap(),
        );

        let mvp_buffer_pool = CpuBufferPool::uniform_buffer(device.clone());

        // mvp_buffer_pool.reserve(((swapchain.num_images() as usize) * mem::size_of::<ModelViewProjection>()) as u64).unwrap();

        // Set up pipeline
        let pipeline = {

            let vs = vs::Shader::load(device.clone()).unwrap();
            let fs = fs::Shader::load(device.clone()).unwrap();

            Arc::new(
                GraphicsPipeline::start()
                    .vertex_input_single_buffer::<mesh::Vertex>()
                    .vertex_shader(vs.main_entry_point(), ())
                    .viewports_dynamic_scissors_irrelevant(1)
                    .fragment_shader(fs.main_entry_point(), ())
                    .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
                    .build(device.clone())
                    .unwrap(),
            )
        };

        let descriptor_set_layout = pipeline.layout().descriptor_set_layouts().get(0).unwrap();
        let descriptor_pool = FixedSizeDescriptorSetsPool::new(descriptor_set_layout.clone());

        let dynamic_state = DynamicState::none();

        let should_recreate_swapchain = false;

        let previous_frame_end = Some(vulkano::sync::now(device.clone()).boxed());

        Renderer {
            instance: instance,
            surface: surface,
            device: device,
            queue: queue,
            swapchain: swapchain,
            images: images,
            render_pass: render_pass,
            pipeline: pipeline,
            dynamic_state: dynamic_state,
            framebuffers: None,
            should_recreate_swapchain: should_recreate_swapchain,
            previous_frame_end: previous_frame_end,
            descriptor_pool: descriptor_pool,
            mvp_buffer_pool: mvp_buffer_pool
        }
    }

    pub fn set_up_framebuffers(&mut self) {
        let dimensions = self.images[0].dimensions();

        let viewport = Viewport {
            origin: [0.0, 0.0],
            dimensions: [dimensions[0] as f32, dimensions[1] as f32],
            depth_range: 0.0..1.0,
        };

        self.dynamic_state.viewports = Some(vec![viewport]);

        self.framebuffers = Some(
            self.images
                .iter()
                .map(|image| {
                    let depth_buffer = AttachmentImage::transient(self.device.clone(), dimensions, Format::D16Unorm).unwrap();
                    Arc::new(
                        Framebuffer::start(self.render_pass.clone())
                            .add(ImageView::new(image.clone()).unwrap())
                            .unwrap()
                            .add(ImageView::new(depth_buffer.clone()).unwrap())
                            .unwrap()
                            .build()
                            .unwrap(),
                    ) as Arc<dyn FramebufferAbstract + Send + Sync>
                })
                .collect::<Vec<_>>(),
        )
    }

    pub fn window_resized(&mut self) {
        self.should_recreate_swapchain = true;
    }

    fn recreate_swapchain(&mut self) -> bool {
        let dimensions: [u32; 2] = self.surface.window().inner_size().into();
        let (new_swapchain, new_images) =
            match self.swapchain.recreate().dimensions(dimensions).build() {
                Ok(r) => r,
                Err(SwapchainCreationError::UnsupportedDimensions) => return false,
                Err(e) => panic!("Failed to recreate swapchain: {:?}", e),
            };

        self.swapchain = new_swapchain;
        self.images = new_images;

        self.set_up_framebuffers();
        self.should_recreate_swapchain = false;

        true
    }

    // FIXME: Fix crippling performance issue (probably due to allocating vertex buffers in each draw call...)
    pub fn draw(&mut self, instances: &Vec<MeshInstance>, camera: &Camera) {
        self.previous_frame_end.as_mut().unwrap().cleanup_finished();

        if self.should_recreate_swapchain {
            if !self.recreate_swapchain() {
                return;
            }
        }

        let (image_num, suboptimal, acquire_future) =
            match swapchain::acquire_next_image(self.swapchain.clone(), None) {
                Ok(r) => r,
                Err(AcquireError::OutOfDate) => {
                    self.should_recreate_swapchain = true;
                    return;
                }
                Err(e) => panic!("Failed to acquire next image: {:?}", e),
            };

        if suboptimal {
            self.should_recreate_swapchain = true;
        }

        // TODO: Draw more than one mesh
        assert_eq!(instances.len(), 1);

        let (vert_buffer, vert_buffer_future) = ImmutableBuffer::from_iter(
            instances[0].mesh.vertices.clone().into_iter(),
            BufferUsage::vertex_buffer(),
            self.queue.clone(),
        ).unwrap();


        let (index_buffer, index_buffer_future) = ImmutableBuffer::from_iter(
            instances[0].mesh.faces.clone().into_iter().flatten().collect::<Vec<u32>>().into_iter(),
            BufferUsage::index_buffer(),
            self.queue.clone()
        ).unwrap();

        let buffer_alloc_future = vert_buffer_future.join(index_buffer_future);

        let mvp = vs::ty::MVPMatrices {
            model: instances[0].get_model_matrix().into(),
            view: camera.get_view_matrix().into(),
            proj: camera.get_projection_matrix().into()
        };

        let mvp_buffer = self.mvp_buffer_pool.next(mvp).unwrap();

        let descriptor_set = Arc::new(self.descriptor_pool.next()
            .add_buffer(mvp_buffer)
            .unwrap()
            .build()
            .unwrap());

        let clear_values = vec![[0.0, 0.0, 0.0, 1.0].into(), 1f32.into()];

        let mut builder = AutoCommandBufferBuilder::primary(
            self.device.clone(),
            self.queue.family(),
            CommandBufferUsage::OneTimeSubmit,
        )
        .unwrap();

        builder
            .begin_render_pass(
                self.framebuffers.as_ref().unwrap()[image_num].clone(),
                SubpassContents::Inline,
                clear_values,
            )
            .unwrap()
            .draw_indexed(
                self.pipeline.clone(),
                &self.dynamic_state,
                vert_buffer.clone(),
                index_buffer.clone(),
                descriptor_set.clone(),
                (),
            )
            .unwrap()
            .end_render_pass()
            .unwrap();

        let command_buffer = builder.build().unwrap();

        let future = self
            .previous_frame_end
            .take()
            .unwrap()
            .join(acquire_future)
            .join(buffer_alloc_future)
            .then_execute(self.queue.clone(), command_buffer)
            .unwrap()
            .then_swapchain_present(self.queue.clone(), self.swapchain.clone(), image_num)
            .then_signal_fence_and_flush();

        match future {
            Ok(future) => {
                self.previous_frame_end = Some(future.boxed());
            }
            Err(FlushError::OutOfDate) => {
                self.should_recreate_swapchain = true;
                self.previous_frame_end = Some(vulkano::sync::now(self.device.clone()).boxed());
            }
            Err(e) => {
                println!("Failed to flush future: {:?}", e);
                self.previous_frame_end = Some(vulkano::sync::now(self.device.clone()).boxed());
            }
        }
    }
}
