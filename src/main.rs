use std::sync::Arc;
use std::time::Instant;
use ultraviolet::{Rotor3, Vec3};
use winit::event::{Event, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::{Fullscreen, WindowBuilder};
use crate::mesh::MeshInstance;
use crate::camera::Camera;

mod renderer;
mod mesh;
mod camera;

fn main() {
    let event_loop = EventLoop::new();
    let window = Arc::new(
        WindowBuilder::new()
            .with_title("Frobenius")
            //.with_fullscreen(Some(Fullscreen::Borderless(None)))
            //.with_decorations(false)
            .build(&event_loop)
            .expect("Failed to create window"),
    );

    let mut renderer = renderer::Renderer::init(None, None, window.clone());
    renderer.set_up_framebuffers();

    println!("Created renderer.");

    //let example_mesh = Arc::new(mesh::load_mesh("src/assets/stanford_dragon.ply"));
    let example_mesh= Arc::new(mesh::example::unit_quad());
    println!("Loaded example mesh.");

    let mut instances = vec![MeshInstance {
        mesh: example_mesh.clone(),
        trans: Vec3::zero(),
        scale: 4.0 * Vec3::one(),
        rot: Rotor3::identity()
    }];

    let camera_pos = Vec3::new(3.0, 3.0, 3.0);
    let camera_look_at = Vec3::zero();
    let mut camera = Camera {
        pos: camera_pos,
        target: camera_look_at,
        up: Camera::calculate_up_vector(camera_pos, camera_look_at),
        horizontal_fov: 100.0 * (std::f32::consts::PI / 180.0), // FIXME: This might be broken/not calculated correctly
        aspect_ratio: 16.0/9.0,
        z_near: 0.1
    };

    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent {
            event: WindowEvent::CloseRequested,
            ..
        } => {
            *control_flow = ControlFlow::Exit;
        }
        Event::WindowEvent {
            event: WindowEvent::Resized(window_size),
            ..
        } => {
            renderer.window_resized();
            camera.aspect_ratio = (window_size.width as f32)/(window_size.height as f32);
        }
        Event::RedrawEventsCleared => {
            //let draw_start = Instant::now();
            renderer.draw(&instances, &camera);
            instances[0].rot = Rotor3::from_rotation_xy(std::f32::consts::PI/200.0) * instances[0].rot;
            //println!("Frame time: {}", draw_start.elapsed().as_secs_f64());
        }
        _ => (),
    });
}
