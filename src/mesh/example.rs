use crate::mesh::{Face, Mesh, Vertex};

pub fn unit_quad() -> Mesh {
    let vertices = vec![
        Vertex::new(-0.5, -0.5, 0.0),
        Vertex::new(-0.5, 0.5, 0.0),
        Vertex::new(0.5, 0.5, 0.0),
        Vertex::new(0.5, -0.5, 0.0),
    ];

    let faces = vec![
        Face {
            vertex_indices: vec![0, 1, 3]
        },
        Face {
            vertex_indices: vec![1, 2, 3]
        }
    ];

    return Mesh {
        vertices, faces
    }
}