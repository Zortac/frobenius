pub mod example;

use std::sync::Arc;
use ply_rs::ply::Property;
use ultraviolet::rotor::Rotor3;
use ultraviolet::mat::Mat4;
use ultraviolet::{Vec3, Vec4};

#[derive(Default, Debug, Copy, Clone)]
pub struct Vertex {
    pub position: [f32; 3],
}

impl Vertex {
    fn new(x: f32, y: f32, z: f32) -> Self {
        Self {
            position: [x, y, z],
        }
    }
}

impl ply_rs::ply::PropertyAccess for Vertex {
    fn new() -> Self {
        Vertex {
            position: [0.0; 3],
        }
    }

    fn set_property(&mut self, _property_name: String, _property: Property) {
        match (_property_name.as_ref(), _property) {
            ("x", ply_rs::ply::Property::Float(v)) => self.position[0] = v,
            ("y", ply_rs::ply::Property::Float(v)) => self.position[1] = v,
            ("z", ply_rs::ply::Property::Float(v)) => self.position[2] = v,
            (k, _) => panic!("Vertex: Unexpected key/value combination: key: {}", k),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Face {
    pub vertex_indices: Vec<u32> // TODO: This should be exactly three indices
}

impl ply_rs::ply::PropertyAccess for Face {
    fn new() -> Self {
        Face {
            vertex_indices: Vec::new()
        }
    }

    fn set_property(&mut self, _property_name: String, _property: Property) {
        match (_property_name.as_ref(), _property) {
            ("vertex_indices", ply_rs::ply::Property::ListUInt(vec)) => self.vertex_indices = vec,
            (k, _) => panic!("Face: Unexpected key/value combination: key: {}", k),
        }
    }
}


impl std::iter::IntoIterator for Face {
    type Item = u32;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.vertex_indices.into_iter()
    }
}

#[derive(Debug)]
pub struct Mesh {
    pub vertices: Vec<Vertex>,
    pub faces: Vec<Face>
}

#[derive(Debug)]
pub struct MeshInstance {
    pub mesh: Arc<Mesh>,
    pub trans: Vec3,
    pub scale: Vec3,
    pub rot: Rotor3
}

impl MeshInstance {
    fn get_translation_matrix(&self) -> Mat4 {
        return Mat4::from_translation(self.trans);
    }

    fn get_scale_matrix(&self) -> Mat4 {
        return Mat4::from_nonuniform_scale_4d(self.scale.into_homogeneous_point());
    }

    fn get_rotation_matrix(&self) -> Mat4 {
        return self.rot.into_matrix().into_homogeneous();
    }

    pub fn get_model_matrix(&self) -> Mat4 {
        // println!("{:?}", self.get_translation_matrix());
        // println!("{:?}", self.get_rotation_matrix());
        // println!("{:?}", self.get_scale_matrix());
        return self.get_translation_matrix() * self.get_rotation_matrix() * self.get_scale_matrix();
    }
}

// FIXME(API): Make this return a Result<Mesh, _>
pub fn load_mesh(path: &str) -> Mesh {
    let f = std::fs::File::open(path).unwrap();
    let mut f = std::io::BufReader::new(f);

    let vertex_parser = ply_rs::parser::Parser::<Vertex>::new();
    let face_parser = ply_rs::parser::Parser::<Face>::new();

    let header = vertex_parser.read_header(&mut f).unwrap();

    let mut mesh = Mesh {
        vertices: Vec::new(),
        faces: Vec::new(),
    };

    for (_, element) in &header.elements {
        match element.name.as_ref() {
            "vertex" => mesh.vertices = vertex_parser.read_payload_for_element(&mut f, &element, &header).unwrap(),
            "face" => mesh.faces = face_parser.read_payload_for_element(&mut f, &element, &header).unwrap(),
            _ => panic!("Enexpeced element!"),
        }
    }

    return mesh;
}