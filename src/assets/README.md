# Acknowledgements
## Stanford Dragon
The 3D scan of a dragon (`stanford_dragon.ply`) is provided by the Stanford Computer Graphics Laboratory.