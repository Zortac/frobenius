#version 450

layout(location = 0) in vec3 position;
layout(location = 0) out vec3 fragColor;

layout(set = 0, binding = 0) uniform MVPMatrices {
    mat4 model;
    mat4 view;
    mat4 proj;
} mvp;

// TODO: Check that uniform buffer works
void main() {
    gl_Position = mvp.proj * mvp.view * mvp.model * vec4(position, 1.0);
    //gl_Position = vec4(position, 1.0);
    fragColor = vec3(1.0);
}