use ultraviolet::{Mat4, Vec3, Mat3};
use ultraviolet::projection::perspective_infinite_z_vk;
use crate::mesh::{MeshInstance};

pub struct Camera {
    pub pos: Vec3,
    pub target: Vec3,
    pub up: Vec3,
    pub horizontal_fov: f32,
    pub aspect_ratio: f32,
    pub z_near: f32
}

impl Camera {
    pub fn get_view_matrix(&self) -> Mat4 {
        return Mat4::look_at(self.pos, self.target, self.up);
    }

    pub fn get_projection_matrix(&self) -> Mat4 {
        let vertical_fov = 2.0 * ((self.horizontal_fov / 2.0).tan() * self.aspect_ratio).atan();
        return perspective_infinite_z_vk(vertical_fov, self.aspect_ratio, self.z_near);
    }

    pub fn calculate_up_vector(position: Vec3, look_at: Vec3) -> Vec3 {
        let connection = position - look_at;
        let normal = connection.cross(Vec3::unit_z());
        let mut eqns = Mat3::new(connection, normal, connection.cross(normal));
        eqns.transpose();
        eqns.inverse();
        let mut up_vec = eqns * Vec3::unit_z();
        up_vec.normalize();
        up_vec = (up_vec.dot(Vec3::unit_z())).signum() * up_vec;
        return up_vec;
    }
}

#[repr(C)]
pub struct ModelViewProjection {
    pub model: Mat4,
    pub view: Mat4,
    pub projection: Mat4,
}

impl ModelViewProjection {
    pub fn from_mesh_and_camera(mesh: MeshInstance, cam: Camera) -> Self {
        return ModelViewProjection {
            model: mesh.get_model_matrix(),
            view: cam.get_view_matrix(),
            projection: cam.get_projection_matrix()
        }
    }
}