# Frobenius
Frobenius is an open-source rendering/game engine framework written in Rust and using Vulkan. (Or at least it aspires to be.)
The project is under heavy development, nowhere near usable and the development may be abandoned at any time.


## TODO Comments:
As the codebase is work-in-progress, there will be numerous `TODO` and `FIXME` comments found in the code.

`TODO` refers to things that should be done eventually while `FIXME` refers to things that need to be fixed ASAP for proper functionality of the software.

These comment may additionally be labeled like `// TODO(LABEL): `, where `LABEL` is one of the following:
- `REF`: General refactoring needs, code smells
- `PERF`: Performance improvements
- `API`: Refactoring related to the publically accessible API of a trait of struct.